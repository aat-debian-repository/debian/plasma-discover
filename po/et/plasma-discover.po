# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Marek Laane <bald@smail.ee>, 2012, 2013, 2014, 2016, 2019, 2020.
# Mihkel Tõnnov <mihhkel@gmail.com>, 2020.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-03-04 09:09+0100\n"
"PO-Revision-Date: 2020-10-11 01:25+0200\n"
"Last-Translator: Mihkel Tõnnov <mihhkel@gmail.com>\n"
"Language-Team: Estonian <>\n"
"Language: et\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.08.1\n"

#: discover/DiscoverObject.cpp:151
#, kde-format
msgid "No application back-ends found, please report to your distribution."
msgstr ""
"Ühtegi rakenduse taustaprogrammi ei leitud, palun anna sellest teada oma "
"distributsioonile."

#: discover/DiscoverObject.cpp:234
#, kde-format
msgid "Could not find category '%1'"
msgstr "Kategooriat \"%1\" ei leitud"

#: discover/DiscoverObject.cpp:268
#, kde-format
msgid ""
"Cannot interact with flatpak resources without the flatpak backend %1. "
"Please install it first."
msgstr ""
"Flatpaki ressurssidega ei saa midagi ette võtta ilma flatpaki "
"taustaprogrammita %1. Palun paigalda see."

#: discover/DiscoverObject.cpp:271 discover/DiscoverObject.cpp:301
#, fuzzy, kde-format
#| msgid "Couldn't open %1"
msgid "Could not open %1"
msgstr "%1 avamine nurjus"

#: discover/DiscoverObject.cpp:299
#, kde-format
msgid "Please make sure Snap support is installed"
msgstr ""

#: discover/main.cpp:36
#, kde-format
msgid "Directly open the specified application by its appstream:// URI."
msgstr "Määratud rakenduse otsene avamine appstream:// URI järgi."

#: discover/main.cpp:37
#, kde-format
msgid "Open with a search for programs that can deal with the given mimetype."
msgstr "Määratud MIME tüüpi käidelda suutvate programmide otsingu avamine."

#: discover/main.cpp:38
#, kde-format
msgid "Display a list of entries with a category."
msgstr "Kirjete loendi kuvamine kategooriatega."

#: discover/main.cpp:39
#, kde-format
msgid "Open Discover in a said mode. Modes correspond to the toolbar buttons."
msgstr ""
"Discoveri avamine määratud režiimis. Režiimid vastavad tööriistariba "
"nuppudele."

#: discover/main.cpp:40
#, kde-format
msgid "List all the available modes."
msgstr "Kõigi saadaolevate režiimide loetlemine."

#: discover/main.cpp:41
#, kde-format
msgid "Compact Mode (auto/compact/full)."
msgstr "Kompaktne režiim (automaatne/kompaktne/täielik)."

#: discover/main.cpp:42
#, kde-format
msgid "Local package file to install"
msgstr "Paigaldatav kohalik paketifail"

#: discover/main.cpp:43
#, kde-format
msgid "List all the available backends."
msgstr "Kõigi saadaolevate taustaprogrammide loetlemine."

#: discover/main.cpp:44
#, kde-format
msgid "Search string."
msgstr "Stringi otsimine."

#: discover/main.cpp:45
#, kde-format
msgid "Lists the available options for user feedback"
msgstr "Kasutaja tagasiside saadaolevate valikute loend"

#: discover/main.cpp:47
#, kde-format
msgid "Supports appstream: url scheme"
msgstr "Appstreami URL-i skeemi toetus"

#: discover/main.cpp:95
#, kde-format
msgid "Discover"
msgstr "Discover"

#: discover/main.cpp:95
#, kde-format
msgid "An application explorer"
msgstr "Rakendusteuurija"

#: discover/main.cpp:96
#, kde-format
msgid "© 2010-2020 Plasma Development Team"
msgstr "© 2010-2020: Plasma arendusmeeskond"

#: discover/main.cpp:97
#, kde-format
msgid "Aleix Pol Gonzalez"
msgstr "Aleix Pol Gonzalez"

#: discover/main.cpp:98
#, kde-format
msgid "Nate Graham"
msgstr "Nate Graham"

#: discover/main.cpp:98
#, kde-format
msgid "Quality Assurance, Design and Usability"
msgstr "Kvaliteedikontroll, disain ja kasutatavus"

#: discover/main.cpp:99
#, kde-format
msgid "Dan Leinir Turthra Jensen"
msgstr "Dan Leinir Turthra Jensen"

#: discover/main.cpp:99
#, kde-format
msgid "KNewStuff"
msgstr "KNewStuff"

#: discover/main.cpp:104
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Marek Laane"

#: discover/main.cpp:105
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "qiilaq69@gmail.com"

#: discover/main.cpp:117
#, kde-format
msgid "Available backends:\n"
msgstr "Saadaolevad taustaprogrammid:\n"

#: discover/main.cpp:163
#, kde-format
msgid "Available modes:\n"
msgstr "Saadaolevad režiimid:\n"

#: discover/qml/AddonsView.qml:18 discover/qml/ApplicationPage.qml:203
#, kde-format
msgid "Addons"
msgstr "Lisad"

#: discover/qml/AddonsView.qml:56
#, kde-format
msgid "More..."
msgstr "Rohkem..."

#: discover/qml/AddonsView.qml:65
#, kde-format
msgid "Apply Changes"
msgstr "Rakenda muudatused"

#: discover/qml/AddonsView.qml:72
#, kde-format
msgid "Reset"
msgstr "Lähtesta"

#: discover/qml/AddSourceDialog.qml:20
#, kde-format
msgid "Add New %1 Repository"
msgstr "Uue %1 hoidla lisamine"

#: discover/qml/AddSourceDialog.qml:58
#, kde-format
msgid "Add"
msgstr "Lisa"

#: discover/qml/AddSourceDialog.qml:70 discover/qml/DiscoverWindow.qml:243
#: discover/qml/InstallApplicationButton.qml:33
#: discover/qml/SourcesPage.qml:140 discover/qml/UpdatesPage.qml:141
#, kde-format
msgid "Cancel"
msgstr "Loobu"

#: discover/qml/ApplicationDelegate.qml:76
#, kde-format
msgctxt "Part of a string like this: '<app name> - <category>'"
msgid "- %1"
msgstr "- %1"

#: discover/qml/ApplicationDelegate.qml:100
#: discover/qml/ApplicationPage.qml:134
#, kde-format
msgid "%1 rating"
msgid_plural "%1 ratings"
msgstr[0] "%1 hinnang"
msgstr[1] "%1 hinnangut"

#: discover/qml/ApplicationDelegate.qml:100
#: discover/qml/ApplicationPage.qml:134
#, kde-format
msgid "No ratings yet"
msgstr "Hinnang veel puudub"

#: discover/qml/ApplicationPage.qml:59
#, kde-format
msgid "Sources"
msgstr "Allikad"

#: discover/qml/ApplicationPage.qml:70
#, kde-format
msgid "%1 - %2"
msgstr "%1 - %2"

#: discover/qml/ApplicationPage.qml:175
#, kde-format
msgid "What's New"
msgstr "Mida on uut"

#: discover/qml/ApplicationPage.qml:212
#, kde-format
msgid "Reviews"
msgstr "Arvustused"

#: discover/qml/ApplicationPage.qml:246
#, kde-format
msgid "Show %1 Review..."
msgid_plural "Show All %1 Reviews..."
msgstr[0] "Näita %1 arvustust ..."
msgstr[1] "Näita kõiki %1 arvustust ..."

#: discover/qml/ApplicationPage.qml:258
#, kde-format
msgid "Write a Review"
msgstr "Kirjuta arvustus"

#: discover/qml/ApplicationPage.qml:258
#, kde-format
msgid "Install to Write a Review"
msgstr "Paigalda ja kirjuta arvustus"

#: discover/qml/ApplicationPage.qml:287
#, kde-format
msgid "Category:"
msgstr "Kategooria:"

#: discover/qml/ApplicationPage.qml:304
#, kde-format
msgid "%1, released on %2"
msgstr "%1, väljalaskeaeg %2"

#: discover/qml/ApplicationPage.qml:311
#, kde-format
msgid "Version:"
msgstr "Versioon:"

#: discover/qml/ApplicationPage.qml:320
#, kde-format
msgid "Author:"
msgstr "Autor:"

#: discover/qml/ApplicationPage.qml:329
#, kde-format
msgid "Size:"
msgstr "Suurus:"

#: discover/qml/ApplicationPage.qml:338
#, kde-format
msgid "Source:"
msgstr "Allikas:"

#: discover/qml/ApplicationPage.qml:347
#, kde-format
msgid "License:"
msgstr "Litsents:"

#: discover/qml/ApplicationPage.qml:355
#, kde-format
msgid "See full license terms"
msgstr "Vaata täielikku litsentsilepingut"

#: discover/qml/ApplicationPage.qml:366
#, kde-format
msgid "Documentation:"
msgstr "Dokumentatsioon:"

#: discover/qml/ApplicationPage.qml:367
#, kde-format
msgid "Read the user guide"
msgstr "Kasutaja käsiraamatu lugemine"

#: discover/qml/ApplicationPage.qml:375
#, kde-format
msgid "Get involved:"
msgstr "Kaasalöömine:"

#: discover/qml/ApplicationPage.qml:376
#, kde-format
msgid "Visit the app's website"
msgstr "Rakenduse kodulehekülje külastamine"

#: discover/qml/ApplicationPage.qml:384
#, kde-format
msgid "Make a donation"
msgstr "Annetuse tegemine"

#: discover/qml/ApplicationPage.qml:392
#, kde-format
msgid "Report a problem"
msgstr "Veast teatamine"

#: discover/qml/ApplicationsListPage.qml:46
#, kde-format
msgid "Search: %1"
msgstr "Otsing: %1"

#: discover/qml/ApplicationsListPage.qml:65
#, kde-format
msgid "Sort: %1"
msgstr "Sortimise alus: %1"

#: discover/qml/ApplicationsListPage.qml:68
#, kde-format
msgid "Name"
msgstr "Nimi"

#: discover/qml/ApplicationsListPage.qml:77
#, kde-format
msgid "Rating"
msgstr "Hinnang"

#: discover/qml/ApplicationsListPage.qml:86
#, kde-format
msgid "Size"
msgstr "Suurus"

#: discover/qml/ApplicationsListPage.qml:95
#, kde-format
msgid "Release Date"
msgstr "Väljalaskeaeg"

#: discover/qml/ApplicationsListPage.qml:139
#, kde-format
msgid "Sorry, nothing found"
msgstr "Andestust, midagi ei leitud"

#: discover/qml/ApplicationsListPage.qml:154
#, kde-format
msgid "Still looking..."
msgstr "Veel otsitakse ..."

#: discover/qml/BrowsingPage.qml:18
#, kde-format
msgid "Featured"
msgstr "Esile tõstetud"

#: discover/qml/BrowsingPage.qml:43 discover/qml/LoadingPage.qml:7
#, kde-format
msgid "Loading..."
msgstr "Laadimine..."

#: discover/qml/BrowsingPage.qml:60
#, kde-kuit-format
msgctxt "@info"
msgid "Unable to load applications.<nl/>Please verify Internet connectivity."
msgstr "Rakenduste laadimine nurjus.<nl/>Palun kontrolli internetiühendust."

#: discover/qml/DiscoverDrawer.qml:99
#, kde-format
msgid "Return to the Featured page"
msgstr "Tagasi esiletõstetute leheküljele"

#: discover/qml/DiscoverWindow.qml:40
#, kde-format
msgid "Running as <em>root</em> is discouraged and unnecessary."
msgstr ""
"Käivitamine administraatorina (<em>root</em>) on äärmiselt ebasoovitatav ja "
"tarbetu."

#: discover/qml/DiscoverWindow.qml:52
#, kde-format
msgid "Home"
msgstr "Kodu"

#: discover/qml/DiscoverWindow.qml:63
#, kde-format
msgid "Search"
msgstr "Otsing"

#: discover/qml/DiscoverWindow.qml:71 discover/qml/InstalledPage.qml:16
#, kde-format
msgid "Installed"
msgstr "Paigaldatud"

#: discover/qml/DiscoverWindow.qml:78
#, kde-format
msgid "Fetching updates..."
msgstr "Uuenduste tõmbamine ..."

#: discover/qml/DiscoverWindow.qml:78
#, kde-format
msgid "Up to date"
msgstr "Värske"

#: discover/qml/DiscoverWindow.qml:78
#, kde-format
msgctxt "Update section name"
msgid "Update (%1)"
msgstr "Uuendused (%1)"

#: discover/qml/DiscoverWindow.qml:85
#, kde-format
msgid "About"
msgstr "Teave"

#: discover/qml/DiscoverWindow.qml:92 discover/qml/SourcesPage.qml:12
#, kde-format
msgid "Settings"
msgstr "Seadistused"

#: discover/qml/DiscoverWindow.qml:136
#, kde-format
msgid "Sorry..."
msgstr "Andestust ..."

#: discover/qml/DiscoverWindow.qml:140
#, kde-format
msgid "Could not close Discover, there are tasks that need to be done."
msgstr "Discoverit ei saa sulgeda, enne peavad tööd tehtud olema."

#: discover/qml/DiscoverWindow.qml:140
#, kde-format
msgid "Quit Anyway"
msgstr "Välju ikkagi"

#: discover/qml/DiscoverWindow.qml:143
#, kde-format
msgid "Unable to find resource: %1"
msgstr "Ressurssi ei leitud: %1"

#: discover/qml/DiscoverWindow.qml:230 discover/qml/SourcesPage.qml:130
#, kde-format
msgid "Proceed"
msgstr "Edasi"

#: discover/qml/Feedback.qml:11
#, kde-format
msgid "Submit usage information"
msgstr "Kasutusteabe edastamine"

#: discover/qml/Feedback.qml:12
#, kde-format
msgid ""
"Sends anonymized usage information to KDE so we can better understand our "
"users. For more information see https://kde.org/privacypolicy-apps.php."
msgstr ""
"Anonüümseks muudetud kasutusteabe saatmine KDE-le, et me mõistaksime "
"paremini oma kasutajaid. Täpsemat teavet leiab aadressilt https://kde.org/"
"privacypolicy-apps.php."

#: discover/qml/Feedback.qml:15
#, kde-format
msgid "Submitting usage information..."
msgstr "Kasutusteabe edastamine ..."

#: discover/qml/Feedback.qml:15
#, kde-format
msgid "Configure"
msgstr "Seadista "

#: discover/qml/Feedback.qml:19
#, kde-format
msgid "Configure feedback..."
msgstr "Seadista tagasisidet ..."

#: discover/qml/Feedback.qml:48
#, kde-format
msgid ""
"You can help us improving this application by sharing statistics and "
"participate in surveys."
msgstr ""
"Sa võid aidata meil rakendust paremaks muuta, kui jagad meiega statistikat "
"ja osaled uuringutes."

#: discover/qml/Feedback.qml:48
#, kde-format
msgid "Contribute..."
msgstr "Panusta ..."

#: discover/qml/Feedback.qml:53
#, kde-format
msgid "We are looking for your feedback!"
msgstr "Me ootame sinu tagasisidet!"

#: discover/qml/Feedback.qml:53
#, kde-format
msgid "Participate..."
msgstr "Osale ..."

#: discover/qml/InstallApplicationButton.qml:14
#, kde-format
msgid "Install"
msgstr "Paigalda"

#: discover/qml/InstallApplicationButton.qml:14
#, kde-format
msgid "Remove"
msgstr "Eemalda"

#: discover/qml/navigation.js:19
#, kde-format
msgid "Resources for '%1'"
msgstr "\"%1\" ressursid"

#: discover/qml/navigation.js:43
#, kde-format
msgid "Extensions..."
msgstr "Laiendid..."

#: discover/qml/ProgressView.qml:17
#, kde-format
msgid "Tasks (%1%)"
msgstr "Edenemine (%1%)"

#: discover/qml/ProgressView.qml:17 discover/qml/ProgressView.qml:40
#, kde-format
msgid "Tasks"
msgstr "Ülesanded"

#: discover/qml/ProgressView.qml:95
#, fuzzy, kde-format
#| msgctxt "TransactioName - TransactionStatus"
#| msgid "%1 - %2: %3"
msgctxt "TransactioName - TransactionStatus: speed, remaining time"
msgid "%1 - %2: %3, %4 remaining"
msgstr "%1 - %2: %3"

#: discover/qml/ProgressView.qml:96
#, fuzzy, kde-format
#| msgctxt "TransactioName - TransactionStatus"
#| msgid "%1 - %2: %3"
msgctxt "TransactioName - TransactionStatus: speed"
msgid "%1 - %2: %3"
msgstr "%1 - %2: %3"

#: discover/qml/ProgressView.qml:97
#, kde-format
msgctxt "TransactioName - TransactionStatus"
msgid "%1 - %2"
msgstr "%1 - %2"

#: discover/qml/ReviewDelegate.qml:60
#, kde-format
msgid "Version: %1"
msgstr "Versioon: %1"

#: discover/qml/ReviewDelegate.qml:60
#, fuzzy, kde-format
#| msgid "Version: %1"
msgid "Version: unknown"
msgstr "Versioon: %1"

#: discover/qml/ReviewDelegate.qml:83
#, kde-format
msgid "unknown reviewer"
msgstr "tundmatu arvustaja"

#: discover/qml/ReviewDelegate.qml:84
#, kde-format
msgid "<b>%1</b> by %2"
msgstr "<b>%1</b>, autor %2"

#: discover/qml/ReviewDelegate.qml:84
#, kde-format
msgid "Comment by %1"
msgstr "%1 kommentaar"

#: discover/qml/ReviewDelegate.qml:132
#, kde-format
msgid "Votes: %1 out of %2"
msgstr "Hääli: %1 / %2"

#: discover/qml/ReviewDelegate.qml:140
#, kde-format
msgid "Useful?"
msgstr "Kas see on kasulik?"

#: discover/qml/ReviewDelegate.qml:152
#, kde-format
msgctxt "Keep this string as short as humanly possible"
msgid "Yes"
msgstr "Jah"

#: discover/qml/ReviewDelegate.qml:168
#, kde-format
msgctxt "Keep this string as short as humanly possible"
msgid "No"
msgstr "Ei"

#: discover/qml/ReviewDialog.qml:23
#, kde-format
msgid "Reviewing %1"
msgstr "%1 arvustus"

#: discover/qml/ReviewDialog.qml:35
#, kde-format
msgid "Rating:"
msgstr "Hinnang:"

#: discover/qml/ReviewDialog.qml:39
#, kde-format
msgid "Name:"
msgstr "Nimi:"

#: discover/qml/ReviewDialog.qml:47
#, kde-format
msgid "Title:"
msgstr "Pealkiri:"

#: discover/qml/ReviewDialog.qml:66
#, kde-format
msgid "Enter a rating"
msgstr "Anna hinnang"

#: discover/qml/ReviewDialog.qml:67
#, kde-format
msgid "Write the title"
msgstr "Kirjuta pealkiri"

#: discover/qml/ReviewDialog.qml:68
#, kde-format
msgid "Write the review"
msgstr "Kirjuta arvustus"

#: discover/qml/ReviewDialog.qml:69
#, kde-format
msgid "Keep writing..."
msgstr "Kirjuta aga edasi ..."

#: discover/qml/ReviewDialog.qml:70
#, kde-format
msgid "Too long!"
msgstr "Liiga pikk!"

#: discover/qml/ReviewDialog.qml:84
#, kde-format
msgid "Submit review"
msgstr "Saada arvustus"

#: discover/qml/ReviewsPage.qml:43
#, kde-format
msgid "Reviews for %1"
msgstr "%1 arvustused"

#: discover/qml/ReviewsPage.qml:54
#, kde-format
msgid "Write a Review..."
msgstr "Kirjuta arvustus ..."

#: discover/qml/ReviewsPage.qml:59
#, kde-format
msgid "Install this app to write a review"
msgstr "Paigalda see rakendus, et arvustus kirjutada"

#: discover/qml/SearchField.qml:27
#, kde-format
msgid "Search..."
msgstr "Otsi..."

#: discover/qml/SearchField.qml:27
#, kde-format
msgid "Search in '%1'..."
msgstr "Otsi kategoorias \"%1\"..."

#: discover/qml/SourcesPage.qml:32
#, kde-format
msgid "%1 (Default)"
msgstr "%1 (vaikimisi)"

#: discover/qml/SourcesPage.qml:51
#, kde-format
msgid "Add Source..."
msgstr "Lisa allikas ..."

#: discover/qml/SourcesPage.qml:76
#, kde-format
msgid "Make default"
msgstr "Vaikimisi"

#: discover/qml/SourcesPage.qml:170
#, kde-format
msgid "Increase priority"
msgstr "Suurenda prioriteeti"

#: discover/qml/SourcesPage.qml:176
#, kde-format
msgid "Failed to increase '%1' preference"
msgstr "\"%1\" eelistuse suurendamine nurjus"

#: discover/qml/SourcesPage.qml:181
#, kde-format
msgid "Decrease priority"
msgstr "Vähenda prioriteeti"

#: discover/qml/SourcesPage.qml:187
#, kde-format
msgid "Failed to decrease '%1' preference"
msgstr "\"%1\" eelistuse vähendamine nurjus"

#: discover/qml/SourcesPage.qml:192
#, kde-format
msgid "Remove repository"
msgstr "Eemalda hoidla"

#: discover/qml/SourcesPage.qml:203
#, kde-format
msgid "Show contents"
msgstr "Sisu näitamine"

#: discover/qml/SourcesPage.qml:242
#, kde-format
msgid "Missing Backends"
msgstr "Puuduvad taustaprogrammid"

#: discover/qml/UpdatesPage.qml:12
#, kde-format
msgid "Updates"
msgstr "Uuendused"

#: discover/qml/UpdatesPage.qml:42
#, kde-format
msgid "Update Issue"
msgstr "Uuenda"

#: discover/qml/UpdatesPage.qml:55
#, kde-format
msgid "OK"
msgstr "OK"

#: discover/qml/UpdatesPage.qml:78
#, kde-format
msgid "Update Selected"
msgstr "Uuenda valitud"

#: discover/qml/UpdatesPage.qml:78
#, kde-format
msgid "Update All"
msgstr "Uuenda kõik"

#: discover/qml/UpdatesPage.qml:120
#, kde-format
msgid "All updates selected (%1)"
msgstr "Kõik uuendused valitud (%1)"

#: discover/qml/UpdatesPage.qml:120
#, kde-format
msgid "%1/%2 update selected (%3)"
msgid_plural "%1/%2 updates selected (%3)"
msgstr[0] "%1/%2 uuendust valitud (%3)"
msgstr[1] "%1/%2 uuendust valitud (%3)"

#: discover/qml/UpdatesPage.qml:195
#, kde-format
msgid "Restart"
msgstr "Taaskäivita"

#: discover/qml/UpdatesPage.qml:275
#, kde-format
msgid "%1"
msgstr "%1"

#: discover/qml/UpdatesPage.qml:291
#, kde-format
msgid "Installing"
msgstr "Paigaldamine"

#: discover/qml/UpdatesPage.qml:322
#, kde-format
msgid "More Information..."
msgstr "Rohkem teavet ..."

#: discover/qml/UpdatesPage.qml:350
#, kde-format
msgctxt "@info"
msgid "Fetching updates..."
msgstr "Uuenduste tõmbamine ..."

#: discover/qml/UpdatesPage.qml:359
#, fuzzy, kde-format
#| msgid "%1 (%2)"
msgid "%1 (%2%)\n"
msgstr "%1 (%2)"

#: discover/qml/UpdatesPage.qml:374
#, kde-format
msgctxt "@info"
msgid "Updates"
msgstr "Uuendused"

#: discover/qml/UpdatesPage.qml:383
#, kde-format
msgctxt "@info"
msgid "The system requires a restart to apply updates"
msgstr "Süsteem nõuab uuenduste rakendamiseks taaskäivitust"

#: discover/qml/UpdatesPage.qml:389 discover/qml/UpdatesPage.qml:395
#: discover/qml/UpdatesPage.qml:401
#, kde-format
msgctxt "@info"
msgid "Up to date"
msgstr "Värske"

#: discover/qml/UpdatesPage.qml:407
#, kde-format
msgctxt "@info"
msgid "Should check for updates"
msgstr "Tuleks kontrollida uuendusi"

#: discover/qml/UpdatesPage.qml:413
#, kde-format
msgctxt "@info"
msgid "It is unknown when the last check for updates was"
msgstr "Pole teada, millal viimati uuenduste olemasolu kontrolliti"