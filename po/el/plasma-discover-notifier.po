# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Dimitris Kardarakos <dimkard@gmail.com>, 2015.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2020-12-22 02:35+0100\n"
"PO-Revision-Date: 2015-11-18 16:13+0200\n"
"Last-Translator: Dimitris Kardarakos <dimkard@gmail.com>\n"
"Language-Team: Greek <kde-i18n-el@kde.org>\n"
"Language: el\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: notifier/DiscoverNotifier.cpp:93
#, kde-format
msgid "View Updates"
msgstr ""

#: notifier/DiscoverNotifier.cpp:178
#, kde-format
msgid "Security updates available"
msgstr "Διαθέσιμες ενημερώσεις ασφαλείας"

#: notifier/DiscoverNotifier.cpp:180
#, kde-format
msgid "Updates available"
msgstr "Διαθέσιμες ενημερώσεις"

#: notifier/DiscoverNotifier.cpp:182
#, kde-format
msgid "System up to date"
msgstr "Το σύστημα είναι ενημερωμένο"

#: notifier/DiscoverNotifier.cpp:184
#, kde-format
msgid "Computer needs to restart"
msgstr ""

#: notifier/DiscoverNotifier.cpp:186
#, kde-format
msgid "Offline"
msgstr ""

#: notifier/DiscoverNotifier.cpp:188
#, kde-format
msgid "Applying unattended updates..."
msgstr ""

#: notifier/DiscoverNotifier.cpp:221
#, kde-format
msgctxt "@action:button"
msgid "Restart"
msgstr ""

#: notifier/DiscoverNotifier.cpp:222
#, kde-format
msgid "Restart is required"
msgstr ""

#: notifier/DiscoverNotifier.cpp:223
#, kde-format
msgid "The system needs to be restarted for the updates to take effect."
msgstr ""

#: notifier/DiscoverNotifier.cpp:244
#, kde-format
msgctxt "@action:button"
msgid "Upgrade"
msgstr ""

#: notifier/DiscoverNotifier.cpp:245
#, fuzzy, kde-format
#| msgid "Updates available"
msgid "Upgrade available"
msgstr "Διαθέσιμες ενημερώσεις"

#: notifier/DiscoverNotifier.cpp:246
#, kde-format
msgid "New version: %1"
msgstr ""

#: notifier/main.cpp:37
#, kde-format
msgid "Discover Notifier"
msgstr ""

#: notifier/main.cpp:37
#, fuzzy, kde-format
#| msgid "System update available"
msgid "System update status notifier"
msgstr "Διαθέσιμη ενημέρωση συστήματος"

#: notifier/main.cpp:38
#, kde-format
msgid "© 2010-2020 Plasma Development Team"
msgstr ""

#: notifier/main.cpp:43
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr ""

#: notifier/main.cpp:44
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr ""

#: notifier/main.cpp:50
#, kde-format
msgid "Replace an existing instance"
msgstr ""

#: notifier/main.cpp:52
#, kde-format
msgid "Do not show the notifier"
msgstr ""

#: notifier/main.cpp:52
#, kde-format
msgid "hidden"
msgstr ""

#: notifier/NotifierItem.cpp:35 notifier/NotifierItem.cpp:36
#, kde-format
msgid "Updates"
msgstr ""

#: notifier/NotifierItem.cpp:50
#, kde-format
msgid "Open Discover..."
msgstr ""

#: notifier/NotifierItem.cpp:53
#, kde-format
msgid "See Updates..."
msgstr ""

#: notifier/NotifierItem.cpp:56
#, kde-format
msgid "Refresh..."
msgstr ""

#: notifier/NotifierItem.cpp:60
#, kde-format
msgid "Restart to apply installed updates"
msgstr ""

#: notifier/NotifierItem.cpp:61
#, kde-format
msgid "Click to restart the device"
msgstr ""

#: notifier/NotifierItem.cpp:63
#, kde-format
msgid "Restart..."
msgstr ""